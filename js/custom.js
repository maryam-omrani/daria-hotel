
let navBarMenu = document.querySelector('header .content .right nav');
let Overlay = document.querySelector('.overlay')
function handelMobileMenu() {
  navBarMenu.classList.toggle("show__menu");
  Overlay.classList.toggle("overlay__show");
  document.body.classList.add("fixed_body");
}
function handleOvaelay() {
  if (navBarMenu.classList.contains("show__menu")) {
    Overlay.classList.remove('overlay__show');
    navBarMenu.classList.remove('show__menu');
    document.body.classList.remove("fixed_body");

  }
}
const swiper = new Swiper('.main__slider .swiper-container', {
  // Optional parameters
 
  loop: true,


  pagination: {
    el: '.swiper-pagination',
  },

 // Navigation arrows
  // navigation: {
  //   nextEl: '.swiper-button-next',
  //   prevEl: '.swiper-button-prev',
  // },

  // And if we need scrollbar
  // scrollbar: {
  //   el: '.swiper-scrollbar',
  // },
});

var swiper2 = new Swiper('.swiper__infinte__loop .swiper-container', {
  slidesPerView: 2,
  spaceBetween: 30,
  slidesPerGroup: 2,
  loop: true,
  loopFillGroupWithBlank: true,
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
});

